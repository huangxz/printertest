#pragma once


// CDenDlg 对话框
#include "Resource.h"
class CDenDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CDenDlg)

public:
	CDenDlg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDenDlg();
	virtual BOOL OnInitDialog();
	virtual void OnFinalRelease();

// 对话框数据
	enum { IDD = IDD_DENDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
public:
	afx_msg void OnBnClickedOk();
//	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnCbnSelchangeCombo200();
};
